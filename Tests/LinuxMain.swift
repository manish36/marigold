import XCTest

import marigoldTests

var tests = [XCTestCaseEntry]()
tests += marigoldTests.allTests()
XCTMain(tests)

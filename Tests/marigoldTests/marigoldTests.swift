import XCTest
@testable import marigold

final class marigoldTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Marigold(text: "Hello, World!").text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
